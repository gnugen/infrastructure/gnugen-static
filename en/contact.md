---
layout: page
lang: en
permalink: /en/contact/
translated_fr: /fr/contact/
---

## Stay updated with our announcements !

* Join our Matrix room for announcements **[#annonces:gnugen.ch](https://matrix.to/#/#annonces:gnugen.ch)**,
* or follow us on Pixelfed (Fediverse) **[gnugen@pixelfed.fr](https://pixelfed.fr/gnugen)**.

## Come to see us or contact us !

In [**CM 0 415**](https://plan.epfl.ch/?room=CM+0+415). Our members will kindly
welcome you... even on week-ends ! Or virtually, at **contact (at) gnugen.ch**
or over our main Matrix room : **[#main:gnugen.ch](https://matrix.to/#/#main:gnugen.ch)**.

You can also reach our committee members at :

* **president (at) gnugen.ch** for the president, currently 
    *Nils Antonovitch*,
* **vicepresident (at) gnugen.ch** for the vice-president, currently 
    *Yaëlle Dutoit*,
* **tresorier (at) gnugen.ch** for the treasurer, currently
    *Harrishan Raveendran*,
* **informatique (at) gnugen.ch** for the it team, currently
    *Jonas Sulzer*,
    *Alain Sinzig* and
    *Yoan Giovannini*,
* **communication (at) gnugen.ch** for the head of communication, currently 
    *TBA*,
* **locaux (at) gnugen.ch** for the responsible of our office room, currently
    *Ismail Sahbane*,
* **logistique (at) gnugen.ch** for logistic related stuff, currently 
    *Alain Sinzig*.

## Join us !

We organise regularly meetings during the semester. More info on our
[wiki](https://wiki.gnugen.ch/reunions/accueil).

## Postal address

We still have one ;-)

```
AGEPoly - GNU Generation
P.a. AGEPoly
Esplanade EPFL
Station 9
CH-1015 Lausanne
```
