---
layout: page
lang: en
permalink: /en/
translated_fr: /fr/
---

# gnugen — committed to protect your digital freedoms

Welcome to __gnugen__'s website! Our mission is to promote the use of
[Free Software](https://gnu.org/philosophy) on the [EPFL](https://www.epfl.ch/)
campus. You can find more information about our missions [here](/en/about/).

## Our next event: Just do git

Come and discover, or learn more about, a great free and open-source tool: Git!
In this series of three events, we will introduce to you the basics of this version
control software, as well as more advanced commands, on March 6th and 13th in CM 1 105,
and March 20th in CM 1 100 at 6:15 PM.
We hope to see you there!

![Here is our beautiful poster for this event: ](/assets/images/posts/2024-2025-spring/Just-do-git.png)
