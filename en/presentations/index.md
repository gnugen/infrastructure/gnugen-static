---
layout: page
lang: en
permalink: /en/presentations/
translated_fr: /fr/presentations/
---

# Presentations

This page lists the presentations and guides related to this semester's events.

 - [Git presentation](https://gnugeneration.epfl.ch/presentation/git-1)
 - [Introduction to free software (fr)](https://gitlab.gnugen.ch/gnugen/presentations/-/raw/master/intro-floss/Talk%20GNU,%20Linux%20&%20Logiciel%20Libre%20V2.odp?inline=false)

## [Introduction to LaTeX and Markdown](/en/ltl/ltl-edition/)

 - [Introduction guide to LaTeX (en)](/assets/presentations/ltl/edition/latex-guide.zip)
 - [LaTeX presentation (fr)](/assets/presentations/ltl/edition/LaTeX.pdf)
 - [Introduction guide to Markdown (en)](/assets/presentations/ltl/edition/markdown-guide.zip)
 - [Markdown presentation (fr)](/assets/presentations/ltl/edition/markdown-slides.zip)
