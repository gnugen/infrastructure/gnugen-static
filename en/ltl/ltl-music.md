---
layout: page
lang: en
permalink: /en/ltl/ltl-music/
translated_fr: /fr/ltl/ltl-music/
---

# Music production

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-music.jpg"/>

 - Music production, by **Raffaele Ancarola**
 - Thursday May 5th, 18:45, CM 1 106

This talk introduces several ways to produce music on GNU/Linux using free
software tools.
