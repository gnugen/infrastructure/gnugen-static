---
layout: page
lang: en
permalink: /en/ltl/ltl-edition/
translated_fr: /fr/ltl/ltl-edition/
---

# Introduction to LaTeX and Markdown

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-edition.png"/>

 - Introduction to LaTeX and Markdown, by **Damien Dervishi** and **David
   Dervishi**
 - Thursday March 24th, 18:15, CM 1 106

This talk introduces LaTeX and Markdown, two popular document composition
languages. You will learn how to write reports, scientific documents, articles
and others while focusing on the content rather than on the presentation of your
document.
