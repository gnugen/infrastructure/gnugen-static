---
layout: page
lang: en
permalink: /en/ltl/ltl-git/
translated_fr: /fr/ltl/ltl-git/
---

# Intermediate git

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-git.png"/>

 - Intermediate git, by **Lucas Crĳns** and **Antoine Fontaine**
 - Thursday April 7th, 18:15, CM 1 106

This talk is a follow-up to the previous [introduction to git](/en/jdl/jdl-git/)
given a couple of weeks before this one. Some more advanced features of git will
be introduced to make it even easier for you to collaborate on projects.
