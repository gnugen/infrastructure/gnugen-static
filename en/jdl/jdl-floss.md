---
layout: page
lang: en
permalink: /en/jdl/jdl-floss/
translated_fr: /fr/jdl/jdl-floss/
---

# Introduction to GNU/Linux and Free Software

<img class="big-picture" src="/assets/images/posts/2022-spring-events/jdl/jdl-floss.png"/>

 - Introduction to GNU/Linux and Free Software, by **Nils Antonovitch**
 - Thursday March 10th, 18:15, CM 1 106

This talk introduces the principles and goals of Free Software. The GNU/Linux
free operating system, variants of which being installable during our
Install Fest, will also be introduced.
