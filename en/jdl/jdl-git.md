---
layout: page
lang: en
permalink: /en/jdl/jdl-git/
translated_fr: /fr/jdl/jdl-git/
---

# Introduction to git

<img class="big-picture" src="/assets/images/posts/2022-spring-events/jdl/jdl-git.png"/>

 - Introduction to git, by **Antoine Fontaine** and **Lucas Crĳns**
 - Thursday March 3rd, 18:15, CM 1 106

This talk introduces you to the basic concepts of git, the world's most popular
version control system. You'll learn how to efficiently collaborate on small- to
large-scale projects.
