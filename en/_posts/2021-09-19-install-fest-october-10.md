---
layout: post
lang: en
translated_fr: /fr/2021/09/19/install-fest-10-octobre.html
title: "Install Fest - Fall 2021"
author: "David Dervishi"
---

Our next *Install Fest* will occur on the 10th of October 2021 in [CM 1 106](https://map.epfl.ch/?room=CM%201%20106)
between 10h00 and 18h00. We will be able to help you install and discover a *GNU/Linux* operating system.

![Install-Fest poster for October 10, 2021](/assets/images/posts/install-fest-fall-2021.png)

Because of the current pandemic, we won't organize our usual potluck.

We hope to see you there!
