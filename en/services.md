---
layout: page
lang: en
permalink: /en/services/
translated_fr: /fr/services/
---

# Services

We offer various services to associations, collaborators and students at EPFL,
as well as services dedicated to our members.

## Associations

We are always happy to help you use free software for your association! More
information on how to contact us about this can be found (in French)
[here](/fr/agep-libre/).

## Students

#### Help for a GNU/Linux installation

We are always available for chatting or to help install a *GNU/Linux* operating
system on your machine. For those who can come to EPFL, you can find us in [CM 0
415](https://plan.epfl.ch/?room=CM+0+415). We are also reachable by other means,
see the [contact page](/en/contact).

<!-- You only have to pass by our room during the day or come to our install
fest (check [latest news](/en/news). Don't hesitate to [contact](/en/contact))
us if you have questions or if your are in trouble, we will do our best to help!
-->

## Members

Some of our services are only accessible to our members. 
We notably offer a Matrix and a GitLab account, as well as other services in the future. 
You can become a member
[here](https://wiki.gnugen.ch/) (in French).
