---
layout: page
lang: en
permalink: /en/about/
translated_fr: /fr/about/
---

Welcome to __gnugen__'s website! Our mission is to promote the use of
[Free Software](https://www.gnu.org/philosophy/free-sw.html) on the
[EPFL](https://www.epfl.ch/) campus. You will find here all there is to know
about our commission, our latest news and events, as well as a
[wiki](https://wiki.gnugen.ch/) containing help and information about the
services we propose to EPFL students and collaborators.

## Free what? 

Free as in Free speech, not free beer! According to the [Free
Software Foundation](https://www.fsf.org), Free Software is software that gives
its users the four following freedoms:

* The freedom to _run_ the software for any purpose. 
* The freedom to _study_ how the software works, and change it to make it do what you wish.
* The freedom to _redistribute_ copies so you can help your neighbour.
* The freedom to _improve_ the software, and release your improvements (and modified versions in general) to the public, so that the whole community benefits.

Using Free Software is a guarantee of _quality_, _security_ and _freedom_ in
our modern world. Free Software is the result of a synergy between users and
developers (those who write programs), as -- unlike in big software shops
where coders will never use what they have written -- the developers are
also users, and any user who wishes it may become a developer. Therefore,
the code is very often the result of collaborative work between many
independant writers that ensures better code quality -- resulting in better
software -- than proprietary software, whose code is not reviewed and tested
by its users, although they are best placed to demand quality of their
software. This same collaboration is to be credited for the high security
offered by Free Software, because numerous independant security experts have
proof-read its code, with their own safe use as an interest. Finally,
as mentioned, Free Software is before all else about respecting users'
freedom and privacy, akin to Human Rights in Informatics.

Moreover, as the software is done by and for the community, it entirely
respects the private life of its users: first because there is no
organisation with economic or political interests behind the software and
secondly because such a lack of elementary decency would be immediately
discovered and discarded by other developers.

## That seems very nice but... where do you guys come in?

Interested in Free Software? Thinking it is time to take your IT freedoms and
your privacy back in your own hands? Then the __gnugen__ can help you
achieve these laudable goals! We can counsel you on the choice of Free Software,
help you install and use it, or offer [services](/en/services/) based on Free
Software that can ease your everyday and student life. We mostly provide help
for installing [GNU/Linux](https://getgnulinux.org/en/linux/), an entirely Free
operating system (the software that makes your computer run, akin to Microsoft
Windows<sup>®</sup> or Mac OS<sup>®</sup>) which will allow you to use your
computer in a secure and free manner. We can also assist you to setup and use
numerous other softwares, such as _git_, _vim_ or _emacs_, _PGP_, etc. Feel free
to come and [meet us](https://plan.epfl.ch/?room=CM+0+415) during the day, or
maybe come to one of our install fests or [contact
us](/en/contact/)!

## But all of this is of no use to me if I'm not a geek, right?

__WRONG__! Free Software is for everyone, it is not required to be a computer
whiz to use it. They even are, more often than not, a lot simpler to use than
their proprietary counterparts due to their stability and their creation by and
for end-users. A good example that many of you probably already use is
[VLC](https://www.videolan.org/vlc/), a video and stream player. Well, it's
FOSS (Free and Open Source Software)! Which is why it is available in so many
languages, with so many features and customisations, while remaining very stable
and fast. _Especially_ if you know nothing in IT, then your back is better
guarded by Free Software than by trusting big corporations who hold money as a
primary interest.

## How can I join you?

If you want to invest yourself further in promoting Free Software at EPFL and/or
are looking for other people who share your passion and spend some good time
together, as well as continuously learn from the diversity of our members, you
can join us! The only requirements are that you should be a student or an EPFL
collaborator, as we are an AGEPoly commission. The easiest way of joining us is
coming to see us during one of our weekly
[meetings](https://wiki.gnugen.ch/reunions/accueil) to meet the commitee and
sign-up. Freedom-loving friend, we are waiting for you!
