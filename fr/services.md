---
layout: page
lang: fr
permalink: /fr/services/
translated_en: /en/services/
---

# Services

Nous proposons plusieurs services aux associations, collaborateur·ice·s et étudiant·e·s de l'EPFL, ainsi que d'autres réservés à nos membres.

## Associations

Nous sommes toujours heureux·ses de t'aider à utiliser des logiciels libres pour ton association ! Tu peux trouver plus
d'informations sur les façons de nous contacter à ce sujet [ici](/fr/agep-libre/).

## Étudiant·e·s

#### Aide à l'installation de GNU/Linux

Nous sommes toujours disponibles pour aider à installer un système d'exploitation *GNU/Linux* sur votre machine. Pour ceux qui peuvent venir sur le campus, nous sommes parfois en [CM 0 415](https://plan.epfl.ch/?room=CM+0+415). Nous restons joignables par d'autres moyens, voir [contact](/fr/contact/).

<!-- Il vous
suffit pour cela de passer à notre local en journée ou de venir à l'une de nos install fests (voir les [dernières
nouvelles](/fr/news)). N'hésitez pas à [nous contacter](/fr/contact) si vous avez des questions ou que votre situation
est particulière, nous ferons de notre mieux pour vous renseigner ! -->

## Membres

Certains de nos services ne sont accessibles qu'aux membres de l'association. 
Nous offrons un compte Matrix, un compte GitLab ainsi que d'autres services dans le futur. 
Tu peux devenir membre [ici](https://wiki.gnugen.ch/).
