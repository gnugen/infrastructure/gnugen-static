---
layout: page
lang: fr
permalink: /fr/presentations/
translated_en: /en/presentations/
---

# Présentations

Cette page regroupe les présentations et guides des évènements de ce semestre.

 - [Présentation de git (en)](https://gnugeneration.epfl.ch/presentation/git-1)
 - [Introduction au logiciel libre](https://gitlab.gnugen.ch/gnugen/presentations/-/raw/master/intro-floss/Talk%20GNU,%20Linux%20&%20Logiciel%20Libre%20V2.odp?inline=false)

## [Introduction à LaTeX et Markdown](/fr/ltl/ltl-edition/)

 - [Guide d'introduction à LaTeX (en)](/assets/presentations/ltl/edition/latex-guide.zip)
 - [Présentation de LaTeX (fr)](/assets/presentations/ltl/edition/LaTeX.pdf)
 - [Guide d'introduction à Markdown (en)](/assets/presentations/ltl/edition/markdown-guide.zip)
 - [Présentation de Markdown (fr)](/assets/presentations/ltl/edition/markdown-slides.zip)
