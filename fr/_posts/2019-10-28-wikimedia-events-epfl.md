---
layout: post
lang: fr
title: "Événements Wikimedia à l'EPFL"
author: "Timothée Floure"
---

[Wikimedia CH](https://wikimedia.ch/en/) organise (avec l'AGEPoly) trois événements sur le campus ce semestre:

  * 2019-10-30, 17h00-20h00: **Workshop Wikipedia** avec Rama et Flor Méchain.
  * 2019-11-13 et 2019-12-04: **Workshop Wikidata et introduction à Wikibase** avec Julieta Arancio et Flor Méchain.

Les détails ainsi que le formulaire d'inscription sont disponibles sur [cette page
(eventbrite.com)](https://www.eventbrite.com/e/wikimedia-workshops-tickets-77143226467).
