---
layout: post
lang: fr
translated_en: /en/2021/09/19/install-fest-october-10.html
title: "Install Fest - Automne 2021"
author: "David Dervishi"
---

Notre prochaine *Install Fest* aura lieu le dimanche 10 Octobre 2021 en
[CM 1 106](https://map.epfl.ch/?dim_floor=1&lang=en&dim_lang=en&tree_groups=centres_nevralgiques%2Cacces%2Cmobilite_reduite%2Censeignement%2Ccommerces_et_services%2Cvehicules%2Cinfrastructure_plan_grp&tree_group_layers_centres_nevralgiques=information_epfl%2Cguichet_etudiants&tree_group_layers_acces=metro&tree_group_layers_mobilite_reduite=&tree_group_layers_enseignement=&tree_group_layers_commerces_et_services=&tree_group_layers_vehicules=&tree_group_layers_infrastructure_plan_grp=batiments_query_plan&baselayer_ref=grp_backgrounds&map_x=2533110&map_y=1152515&map_zoom=14),
de 10h00 à 18h00. Nous pourrons t'y aider à installer et découvrir un système d'exploitation *GNU/Linux*.

![Affiche de l'Install-Fest du 10 Octobre 2021](/assets/images/posts/install-fest-fall-2021.png)

À cause de la situation sanitaire actuelle, l'habituel pic-nic canadien n'aura pas lieu.

Au plaisir de te voir!
