---
layout: post
lang: fr
title: "Install Fest - Automne 2019"
author: "Timothée Floure"
---

Notre prochaine [*Install Fest*](https://wiki.gnugen.ch/if/accueil) aura lieu le dimanche 13 Octobre 2019 en
[CM.1.106](https://map.epfl.ch/?dim_floor=1&lang=en&dim_lang=en&tree_groups=centres_nevralgiques%2Cacces%2Cmobilite_reduite%2Censeignement%2Ccommerces_et_services%2Cvehicules%2Cinfrastructure_plan_grp&tree_group_layers_centres_nevralgiques=information_epfl%2Cguichet_etudiants&tree_group_layers_acces=metro&tree_group_layers_mobilite_reduite=&tree_group_layers_enseignement=&tree_group_layers_commerces_et_services=&tree_group_layers_vehicules=&tree_group_layers_infrastructure_plan_grp=batiments_query_plan&baselayer_ref=grp_backgrounds&map_x=533110&map_y=152515&map_zoom=14),
de 10h00 à 18h00. Une présentation *Linux, qu'est-ce que c'est ?* donnant une vue d'ensemble de l'environnement
GNU/Linux aura très probablement lieu vers 13h ou 14h
([slides](https://gitlab.gnugen.ch/gnugen/presentations/tree/master/linux-what-is-that)).

![Affiche de l'Install-Fest du 13 Octobre 2019](/assets/images/posts/install-fest-fall-2019.png)

Le repas du midi s'organisera comme d'habitude autour d'un [pic-nic canadien](https://en.wikipedia.org/wiki/Potluck), ce
qui signifie que chacun·e apporte un petit quelque chose à partager (on ne vous mettra pas dehors si vous n'avez rien à
apporter et il y a généralement bien assez à manger).

Au plaisir de vous voir dimanche!

**PS:** les portes du bâtiments sont fermées le dimanche (accès par carte CAMIPRO), n'hésitez pas à [nous
contacter](/fr/contact/) si vous n'êtes pas membre de l'EPFL.
