---
layout: post
lang: fr
title: "Geekerie - 14 Décembre 2019"
author: "Ulysse Widmer"
---

Notre prochaine [Geekerie](https://wiki.gnugen.ch/geekerie) aura lieu le samedi 14 Décembre 2019 en
[CM.1.100](https://plan.epfl.ch/?dim_floor=1&lang=fr&dim_lang=fr&tree_groups=centres_nevralgiques%2Cacces%2Cmobilite_reduite%2Censeignement%2Ccommerces_et_services%2Cvehicules%2Cinfrastructure_plan_grp&tree_group_layers_centres_nevralgiques=information_epfl%2Cguichet_etudiants&tree_group_layers_acces=metro&tree_group_layers_mobilite_reduite=&tree_group_layers_enseignement=&tree_group_layers_commerces_et_services=&tree_group_layers_vehicules=&tree_group_layers_infrastructure_plan_grp=batiments_query_plan&baselayer_ref=grp_backgrounds&map_x=533191&map_y=152515&map_zoom=14),
de 10h00 à 18h00.

<img src="/assets/images/posts/geekerie-2019-12-14-affiche.png" alt="affiche de la geekerie du 14 décembre 2019" width="100%">

Le repas du midi s'organisera comme d'habitude autour d'un [pic-nic canadien](https://en.wikipedia.org/wiki/Potluck), ce
qui signifie que chacun·e apporte un petit quelque chose à partager (on ne vous mettra pas dehors si vous n'avez rien à
apporter et il y a généralement bien assez à manger).

Les présentations suivantes auront lieu dès 13h :
  * _Shell Scripting_, par André Kiepe à 13h
  * _Debian, disappearances and real Software Freedom_, par Daniel Pocock à 14h
  * _OSTree-based systems_, par Timothée Floure a.k.a. Fnux à 15h
  * _Advanced Git_, par Roosembert Palacios a.k.a. Orbstheorem à 16h

Les heures indiquées pour les présentations sont indicatives et peuvent un peu varier en fonction du temps qu'elles vont prendre et de la disponibilité des présentateurs !

Tout le monde peut venir tout au long de la journée (entrée libre) pour participer au repas, aux présentations, ou venir signer sa clé GPG.

Au plaisir de vous voir !
