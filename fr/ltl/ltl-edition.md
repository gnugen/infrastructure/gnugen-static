---
layout: page
lang: fr
permalink: /fr/ltl/ltl-edition/
translated_en: /en/ltl/ltl-edition/
---

# Introduction à LaTeX et Markdown

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-edition.png"/>

 - Introduction à LaTeX et Markdown, par **Damien Dervishi** et **David
   Dervishi**
 - Jeudi 24 mars à 18h15 en CM 1 106

Ce talk introduit LaTeX et Markdown, deux langages très répandus de
composition de documents divers. Vous y apprendrez comment écrire des rapports,
documents scientifiques ou articles en vous concentrant sur le contenu plutôt
que sur la forme.
