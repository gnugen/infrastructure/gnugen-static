---
layout: page
lang: fr
permalink: /fr/ltl/ltl-git/
translated_en: /en/ltl/ltl-git/
---

# git Intermédiaire

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-git.png"/>

 - git Intermédiaire, par **Lucas Crĳns** et **Antoine Fontaine**
 - Jeudi 7 avril à 18h15 en CM 1 106

Ce talk suit la précédente [introduction à git](/fr/jdl/jdl-git/) donnée
quelques semaines plus tôt. Des fonctionnalités plus avancées de git sont au
menu pour augmenter votre efficacité en collaboration.
