---
layout: page
lang: fr
permalink: /fr/ltl/ltl-music/
translated_en: /en/ltl/ltl-music/
---

# Production de musique

<img class="big-picture" src="/assets/images/posts/2022-spring-events/ltl/ltl-music.jpg"/>

 - Production de musique, par **Raffaele Ancarola**
 - Jeudi 5 mai à 18h45 en CM 1 106

Ce talk présente différentes manières de produire de la musique sur GNU/Linux
avec des outils libres.
