---
layout: page
lang: fr
permalink: /fr/
translated_en: /en/
---

# gnugen — s'engage à protéger tes libertés numériques

Bienvenue sur le site de la __gnugen__, la commission de l'AGEPoly de promotion du [logiciel
libre](https://fr.wikipedia.org/wiki/Logiciel_libre) sur le campus de l'[EPFL](https://www.epfl.ch/). Plus d'informations sur nos missions [ici](/fr/about/).

## Notre prochain événement: Just do git

Viens découvrir ou en apprendre plus sur un super outil libre et open source : git ! Dans cette
série de trois évènements, nous te présenterons les bases de ce logiciel de versionnage, ainsi
que des commandes plus avancées, les 6 et 13 mars en CM 1 105 et le 20 mars en CM 1 100 à 18h15.
Nous espérons vous y voir !

![Voici notre belle affiche : ](/assets/images/posts/2024-2025-spring/Just-do-git.png)
