---
layout: page
lang: fr
permalink: /fr/contact/
translated_en: /en/contact/
---

## Reste au courant avec nos annonces !

* Rejoins notre salon Matrix pour les annonces **[#annonces:gnugen.ch](https://matrix.to/#/#annonces:gnugen.ch)**,
* ou suis nous sur Pixelfed (Fediverse) **[gnugen@pixelfed.fr](https://pixelfed.fr/gnugen)**.

## Passez nous voir ou contactez-nous !

Physiquement, en [**CM 0 415**](https://plan.epfl.ch/?room=CM+0+415). Des membres vous accueilleront volontiers... même les week-ends ! Ou virtuellement, à l'adresse : **contact (arobase) gnugen (point) ch** ou dans notre salon Matrix principal : **[#main:gnugen.ch](https://matrix.to/#/#main:gnugen.ch)**.

Vous pouvez contacter les membres du comité aux adresses suivantes :

* **president (arobase) gnugen.ch** pour le président, actuellement 
    *Nils Antonovitch*,
* **vicepresident (arobase) gnugen.ch** pour le vice-président, actuellement 
    *Yaëlle Dutoit*,
* **tresorier (arobase) gnugen.ch** pour le trésorier, actuellement
    *Harrishan Raveendran*,
* **informatique (arobase) gnugen.ch** pour les responsables informatique, actuellement 
    *Jonas Sulzer*,
    *Alain Sinzig* et
    *Yoan Giovannini*,
* **communication (arobase) gnugen.ch** pour les responsables communication, actuellement 
    *TBA*,
* **locaux (arobase) gnugen.ch** pour le responsable du local, actuellement
    *Ismail Sahbane*,
* **logistique (arobase) gnugen.ch** pour le responsable logistique, actuellement
    *Alain Sinzig*.

## Nous rejoindre !

Nous organisons régulièrement des réunions en cours de semestre. Toutes les infos sur notre [wiki](https://wiki.gnugen.ch/reunions/accueil).


## Adresse postale

Nous en avons encore une ;-)

```
AGEPoly - GNU Generation
P.a. AGEPoly
Esplanade EPFL
Station 9
CH-1015 Lausanne
```
