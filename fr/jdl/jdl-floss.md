---
layout: page
lang: fr
permalink: /fr/jdl/jdl-floss/
translated_en: /en/jdl/jdl-floss/
---

# Introduction à GNU/Linux et au Logiciel Libre

<img class="big-picture" src="/assets/images/posts/2022-spring-events/jdl/jdl-floss.png"/>

 - Introduction à GNU/Linux et au Logiciel Libre, par **Nils Antonovitch**
 - Jeudi 10 mars à 18h15 en CM 1 106

Ce talk introduit les principes et buts du Logiciel Libre. Le système
d'exploitation libre GNU/Linux, dont des variantes pourront être installées
pendant notre Install Fest, y sera aussi présenté.
