---
layout: page
lang: fr
permalink: /fr/jdl/jdl-git/
translated_en: /en/jdl/jdl-git/
---

# Introduction à git

<img class="big-picture" src="/assets/images/posts/2022-spring-events/jdl/jdl-git.png"/>

 - Introduction à git, par **Antoine Fontaine** et **Lucas Crĳns**
 - Jeudi 3 mars à 18h15 en CM 1 106

Ce talk a pour but d'introduire les concepts de base de git, le système de
gestion de version le plus utilisé au monde. Vous y apprendrez à collaborer
efficacement sur des projets à petite ou grande échelle!
