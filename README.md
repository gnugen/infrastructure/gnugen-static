# gnugen website

This repository contains the sources for the association's [website](https://gnugen.ch)
- not to be confused with the [wiki](https://wiki.gnugen.ch)!
The website is rendered using [jekyll](https://jekyllrb.com/) and contains:

* A presentation of the association, as well as means to contact us
* News about the events we organise
* Other stuff?

## (tempoarary) Upload to production (pipeline doesn't work)

Connect to server and become root
```bash
ssh gnu-web
sudo -i
```

Go to repo and pull the repo:
```bash
cd nginx/data/gnugen-static/
git pull
```

Build the website
```bash
jekyll build
```


## Contributing

If you have something you want to change on the website, you can either use the
edit branch, or if you are doing a major rewrite or what create your own. When
you want your edits sent to prod, just open a merge request to the master
branch and one the admins will proof-read it and merge it. Feel free to
participate!

News are expected to be put on the front page. To add them to the rss and to
have them in /news (the page listing them has been removed), the articles must be named
`/lang/_posts/YYYY-MM-DD-TITLE.md`, and must start with the following preamble:

```
---
layout: post
lang: en
title: "my awesome title"
author: "my very cool name"
---
```

__NOTE__: i18n stuff is to be written in /lang/XY.md: For instance, English in
/en/XY.md, where XY is the _ENGLISH_ title of the page. Use existing articles
as an example.


## Testing

To test your modifications on the rendered pages, simply install Jekyll from
the [official documentation](https://jekyllrb.com/docs/) and then run `jekyll serve` in the
directory containing this README file. You can open the link printed on stdout
to view the website.

You can also use `jekyll serve --watch` to have the pages automatically
rendered each time you same them.

If Jekyll dies by saying a bunch of garbage containing `cannot load such file
-- listen (LoadError)` you'll need to install `ruby-listen`.


## License

This website, like the wiki, is licensed under the terms of
[CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/).
By editing it you aggree that your work to be placed under the same license terms.
