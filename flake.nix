{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";

  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      nacl-hacks = pkgs.callPackage ./nacl-hacks.nix {};
    in {
      devShells.x86_64-linux.default = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [ jekyll ];
      };

      checks.x86_64-linux = {
        checking-404 = pkgs.runCommand "checking-404" {
          src = pkgs.lib.cleanSource ./.;
          nativeBuildInputs = [ pkgs.jekyll pkgs.darkhttpd pkgs.wget ];
        } ''
          mkdir $out
          cp $src/* . -r
          jekyll build --strict_front_matter
          darkhttpd _site --ipv6 --addr 127.0.0.1 &>/dev/null &
          pid="$!"
          mkdir wget-tmp
          cd wget-tmp
          wget -r -nH -nd -l inf -p 'http://127.0.0.1:8080/' &> $out/wget-log || :
          if grep "ERROR 404: Not Found." $out/wget-log -B3 | grep -v Reusing | grep -v 'HTTP request sent' | tee $out/summary; then
            cat -n $out/wget-log
            printf "\n\nSummary:\n"
            cat -n $out/summary
            printf "\nSome links in the website aren't reachable.\n" | tee -a $out/summary
            status=1
          else
            printf "No 404 in website, success.\n" | tee -a $out/summary
            status=0
          fi
          kill "$pid"
          exit $status
        '';
      };

      apps.x86_64-linux = {
        serve = {
          type = "app";
          program = "${pkgs.writeScript "jekyll-serve" "${pkgs.jekyll}/bin/jekyll serve"}";
        };
      };
    };
}
